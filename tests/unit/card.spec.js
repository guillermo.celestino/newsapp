import { shallowMount } from '@vue/test-utils'
import Card from '@/components/Card.vue'

describe('Card.vue', () => {
  it('renders props.allNews when passed', () => {
    const allNews = [
        {
            title: 'News Title',
            author: 'News Author',
            content: 'News Content',
        }
    ];

    const wrapper = shallowMount(Card, {
      props: { allNews }
    })

    expect(wrapper.props().allNews[0].title).toBe('News Title')
    expect(wrapper.props().allNews[0].author).toBe('News Author')
    expect(wrapper.props().allNews[0].content).toBe('News Content')
  })
})
