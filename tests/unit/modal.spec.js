import { shallowMount } from '@vue/test-utils'
import Modal from '@/components/Modal.vue'

describe('Modal.vue', () => {
  it('renders props.open when passed', () => {
    const open = true
    const wrapper = shallowMount(Modal, {
      props: { open }
    })
    expect(wrapper.props().open).toBe(true)
  })
  it('renders props.news when passed', () => {
    const news = {
        title: 'News Title',
        author: 'News Author',
        content: 'News Content',
    }
    const wrapper = shallowMount(Modal, {
      props: { news }
    })
    expect(wrapper.props().news.title).toBe('News Title')
    expect(wrapper.props().news.author).toBe('News Author')
    expect(wrapper.props().news.content).toBe('News Content')
  })
})
