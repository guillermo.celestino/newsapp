import { createStore } from 'vuex'
import axios from 'axios'

const NEWS_API_KEY = "e4e7a7f7627445a5a6adf2306560f362"

export default createStore({
  state: {
    news: [],
    totalResults: null,
    categories:[
      'Business',
      'Entertainment',
      'General',
      'Health',
      'Science',
      'Sports',
      'Technology',
    ],
    countries: []
  },
  mutations: {
    SET_NEWS(state, news) {
      state.news = news
    },
    SET_TOTAL_RESULT(state, total) {
      state.totalResults = total
    },
    SET_COUNTRIES(state, countries) {
      state.countries = countries
    },
  },
  actions: {
    async getNews({ commit }, {country , category , query, page, pageSize}) {

      let url = `http://newsapi.org/v2/top-headlines?country=${country}&category=${category}&page=${page}&pageSize=${pageSize}&apiKey=${NEWS_API_KEY}`

      if(query !== '') {
        url = `http://newsapi.org/v2/top-headlines?country=${country}&category=${category}&q=${query}&page=${page}&pageSize=${pageSize}&apiKey=${NEWS_API_KEY}`
      }

      await axios.get(url)
        .then((r) => {
          const { articles, totalResults }  = r.data
          commit('SET_NEWS', articles)
          commit('SET_TOTAL_RESULT', totalResults)
        })
        .catch((e) => {
          console.log('error', e)
        })
    },
    async getCountries({ commit} ) {
      const url = `https://restcountries.eu/rest/v2/all`

      await axios.get(url)
        .then((r) => {
          commit('SET_COUNTRIES', r.data)
        }).catch((e) => {
          console.log(e)
        })
    }
  },
  modules: {
  }
})
